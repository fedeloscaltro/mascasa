from crispy_forms.layout import Submit
from django.core.exceptions import ValidationError
from users.models import Client
from crispy_forms.helper import FormHelper
from django.contrib.auth.forms import UserCreationForm
from django import forms


class SignUpUserForm(UserCreationForm):
    helper = FormHelper()
    helper.form_method = 'POST'
    helper.add_input(Submit('save', 'Sign up', css_class="btn btn-success"))

    def __init__(self, *args, **kwargs):
        super(SignUpUserForm, self).__init__(*args, **kwargs)
        owners = Client.objects.filter(owner=True)
        if len(owners) > 0:
            self.fields['owner'].widget.attrs['disabled'] = True

    class Meta(UserCreationForm.Meta):
        model = Client
        fields = ['username', 'address', 'phone', 'email', 'owner']

    def clean(self):
        clean_data = self.cleaned_data
        owners = Client.objects.filter(owner=True)
        if clean_data["owner"] and len(owners) > 0:
            raise ValidationError("There cannot be more than one owner!")

        return clean_data


class UpdateUserForm(forms.ModelForm):
    helper = FormHelper()
    helper.form_method = 'POST'
    helper.form_class = "w-50 p-5 align-items-center"
    helper.add_input(Submit('save', 'Update data', css_class="btn btn-success mt-2"))

    class Meta:
        model = Client
        fields = ['username', 'phone', 'email', 'address']
