from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.views.generic import CreateView, UpdateView, DeleteView
from .forms import SignUpUserForm, UpdateUserForm
from users.models import Client


class UserCreateView(CreateView):
    form_class = SignUpUserForm
    template_name = 'users/registration/CRUD/user_create.html'
    success_url = reverse_lazy('homepage')


class UserUpdateView(LoginRequiredMixin, UpdateView):
    model = Client
    template_name = 'users/registration/CRUD/user_update.html'
    success_url = reverse_lazy('homepage')
    form_class = UpdateUserForm

    def get_queryset(self):
        """
        Overridden method to give a user the access to a personal page.
        :return: If the user tries to access other's personal page it will return a 404 status code
        """
        return super(UserUpdateView, self).get_queryset().filter(id=self.request.user.id)


class UserDeleteView(LoginRequiredMixin, DeleteView):
    model = Client
    template_name = 'users/registration/CRUD/user_delete.html'
    success_url = reverse_lazy('homepage')

    def get_queryset(self):
        """
        Overridden method to give a user the access to a personal page.
        :return: If the user tries to access other's personal page it will return a 404 status code
        """
        return super(UserDeleteView, self).get_queryset().filter(id=self.request.user.id)
