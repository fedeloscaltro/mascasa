from django.contrib import admin
from .models import Client


@admin.register(Client)
class ClientAdmin(admin.ModelAdmin):
    list_display = ["username", "email", "owner"]
    fields = ['username', 'email', 'phone', 'address']

    def get_form(self, request, obj=None, change=False, **kwargs):
        form = super(ClientAdmin, self).get_form(request, obj, **kwargs)
        form.base_fields['address'].label = "Living address"  # to customise the label of the address field
        return form
