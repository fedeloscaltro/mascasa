from django.contrib.auth.models import AbstractUser
from django.db import models
from phonenumber_field.modelfields import PhoneNumberField
from CasAvanguardia.constants import MAX_LENGTH


class Client(AbstractUser):
    phone = PhoneNumberField(unique=True)
    email = models.EmailField()
    address = models.CharField(max_length=MAX_LENGTH)
    owner = models.BooleanField(default=False)
