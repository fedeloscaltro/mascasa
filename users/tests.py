from django.test import TestCase, Client
from django.urls import reverse, reverse_lazy

from users.models import Client as custom_client


class BaseTest(TestCase):
    def setUp(self):
        self.credentials = {
            'username': 'testuser',
            'password': 'secret',
            'address': 'Via di Test 2, Reggio Emilia',
            'phone': '+393477777777',
            'email': 'test@test.com',
            'owner': 'False'
        }


class LoginTest(BaseTest):
    def setUp(self):
        super(LoginTest, self).setUp()
        self.login_url = reverse('users:login')
        custom_client.objects.create_user(**self.credentials)

    def test_login_success(self):
        """
        Test to check the condition where a user can log in successfully
        :return: OK if the status code is 200 and if the user is authenticated
        """
        response = self.client.post(self.login_url, self.credentials, follow=True)
        self.assertTrue(response.context['user'].is_authenticated)
        self.assertEqual(response.status_code, 200)

    def test_login_fail(self):
        """
        Test to check the condition where a user fail to log in
        :return: OK if the response correctly is False
        """
        response = self.client.login(username='utentefasullo', password='passwordfasulla')
        self.assertFalse(response)

    def test_login_access_page(self):
        """
        Test to check the ability to access the log in template
        :return: OK if the template used by the response is the one about the log in
        """
        response = self.client.get(self.login_url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'users/registration/login.html')


class SignupTest(BaseTest):
    def setUp(self):
        self.register_url = reverse('users:user-create')
        self.credentials = {
            'username': 'testuser',
            'password': 'secretpassword',
            'address': 'Via di Test 2, Reggio Emilia',
            'phone': '+393477777777',
            'email': 'test@test.com',
            'owner': 'False'
        }

    def test_signup_access_page(self):
        """
        Test to check the ability to access the sign up template
        :return: OK if the template used by the response is the one about the sign up
        """
        response = self.client.get(self.register_url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'users/registration/CRUD/user_create.html')

    def test_signup_form(self):
        """
        Test to check is the user now has been registered to the website
        :return: OK if there wuold have been a redirection and in the DB there's one user
        """
        response = self.client.post(reverse_lazy('users:user-create'),
                                    data={
                                        'username': self.credentials["username"],
                                        'password1': self.credentials["password"],
                                        'password2': self.credentials["password"],
                                        'address': self.credentials['address'],
                                        'email': self.credentials["email"],
                                        'phone': self.credentials["phone"],
                                        'owner': self.credentials["owner"]
                                    }, follow=True)
        self.assertEqual(response.status_code, 200)
        users = custom_client.objects.all()
        self.assertEqual(users.count(), 1)


class LogoutTest(BaseTest):
    def setUp(self):
        super(LogoutTest, self).setUp()
        self.logout_url = reverse('users:logout')
        self.client = Client()

    def test_logout_access_page(self):
        """
        Test to check the ability to access the log out template
        :return: OK if the template used by the response is the one about the log out
        """
        response = self.client.get(self.logout_url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'users/registration/logged_out.html')

    def test_logout(self):
        """
        Test to check the logout functionality
        :return: OK if the template of the response contains the word 'Logout'
        """
        self.client.login(username=self.credentials['username'], password=self.credentials['password'])
        response = self.client.get(reverse('users:logout'))
        self.assertEquals(response.status_code, 200)
        self.assertTrue(b'Logout' in response.content)


class UpdateUserTest(BaseTest):
    def setUp(self):
        super(UpdateUserTest, self).setUp()
        self.created_user = custom_client.objects.create_user(**self.credentials)

    def test_user_update(self):
        """
        Test to check if the user's data have been updated
        :return: OK if username is the one the test lastly filled in
        """
        client = Client()
        response = client.login(username=self.credentials['username'], password=self.credentials['password'])
        self.assertTrue(response)

        response = client.post(
            reverse_lazy('users:user-update', kwargs={'pk': self.created_user.pk}),
            data={
                'username': 'testuser2',
                'address': 'Via di Test 2, Reggio Emilia',
                'phone': '+39347777777',
                'email': 'test@test.com',
                'password': 'secret',
                'owner': 'False'
            }, follow=True
        )
        self.assertEqual(response.status_code, 200)
        self.created_user.refresh_from_db()
        self.assertEqual(self.created_user.username, 'testuser2')

    def test_user_change_password(self):
        """
        Test to check if the user's password has been updated
        :return: OK if the response of the POST request has been accomplished
        """
        client = Client()
        response = client.login(username=self.credentials['username'], password=self.credentials['password'])
        self.assertTrue(response)

        response = client.post(
            reverse_lazy('users:change_password'),
            data={
                'old_password': self.credentials['password'],
                'new_password1': 'nuova_password',
                'new_password2': 'nuova_password'
            }, follow=True
        )
        self.assertEqual(response.status_code, 200)


class DeleteUserTest(BaseTest):

    def setUp(self):
        super(DeleteUserTest, self).setUp()
        self.created_user = custom_client.objects.create_user(**self.credentials)
        self.client = Client()
        self.client.force_login(user=self.created_user)

    def test_user_cancellation(self):
        """
        Test to check the cancellation functionality
        :return: OK if the users table is empty
        """
        response = self.client.delete(reverse_lazy('users:user-delete', kwargs={'pk': self.created_user.pk}), follow=True)
        self.assertEqual(response.status_code, 200)
        users = custom_client.objects.all()
        self.assertEqual(users.count(), 0)
