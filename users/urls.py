from django.conf.urls import url
from django.urls import path, reverse_lazy
from users.views import UserCreateView, UserUpdateView, UserDeleteView
from django.contrib.auth import views as auth_views
app_name = 'users'  # all the names in urlpattern will be preceded by this value

urlpatterns = [
    path('user_create/', UserCreateView.as_view(), name="user-create"),
    path('<int:pk>/user_update/', UserUpdateView.as_view(), name="user-update"),
    path('<int:pk>/user_delete/', UserDeleteView.as_view(), name="user-delete"),
    path('login/', auth_views.LoginView.as_view(template_name='users/registration/login.html'), name='login'),
    path('logout/', auth_views.LogoutView.as_view(template_name='users/registration/logged_out.html'), name='logout'),
    path('password/', auth_views.PasswordChangeView.as_view(template_name="users/registration/CRUD/change_password.html",
                                                            success_url=reverse_lazy('homepage')),
         name='change_password'),
]
