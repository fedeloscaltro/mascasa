from realestate_activity.views import AppointmentView, DeleteAppointmentView
from .settings import MEDIA_URL, MEDIA_ROOT
from django.contrib import admin
from django.urls import path, include
from CasAvanguardia.views import Homepage, contact_view, self_candidacy
from django.conf.urls.static import static

urlpatterns = [
    path('', Homepage.as_view(), name='homepage'),
    path('contact', contact_view, name='contacts'),
    path('lavora-con-noi', self_candidacy, name='work-together'),
    path('appointment', AppointmentView.as_view(), name='appointment'),
    path('appointment/delete-appointments', DeleteAppointmentView.as_view(), name='delete-appointment'),
    path('admin/', admin.site.urls),
    path('user/', include('users.urls')),
    path('activity/', include('realestate_activity.urls')),
] + static(MEDIA_URL, document_root=MEDIA_ROOT)
