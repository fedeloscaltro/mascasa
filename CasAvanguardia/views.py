import logging
from django.core.mail import EmailMessage, send_mail
from django.db.models import Avg
from django.shortcuts import render
from django.views.generic import TemplateView
from CasAvanguardia import settings
from realestate_activity.models import Agent, Rating, Property
from django.contrib import messages
from .constants import CONTRACTS

_logger = logging.getLogger(__name__)


def get_top_agents():
    """
    Function to return the 3 agents with the highest average rate
    :return: a Queryset object containing the data related to the top 3 agents
    """
    rr = Rating.objects.values('fk_a').annotate(avg_score=Avg('score')).order_by('-avg_score')[:3]
    agents_id = [r['fk_a'] for r in rr]  # recovering the id of each top agent
    top_agents = Agent.objects.filter(id_a__in=agents_id)
    return top_agents


def get_latest_properties(context):
    """
    Function to return the latest announcements the owner has released
    :param context: the context variable in which I'm going to insert a special field for that kind of properties
    :return: the new context with the 'latest_properties' key added
    """
    latest_properties = Property.objects.order_by('-insertion_date')[:5]
    context['latest_properties'] = latest_properties
    for prop in context['latest_properties']:
        prop.contr = CONTRACTS[int(prop.contract)]  # inserts for each property the string name of the contract

    return context


class Homepage(TemplateView):
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        context = super(Homepage, self).get_context_data(**kwargs)
        context['top_agents'] = get_top_agents()

        context = get_latest_properties(context)

        return context


def contact_view(request):
    """
    Function to process the request made to contact the real estate agency
    :param request: the request object
    :return: an HttpResponse whose content is filled with the result of the template contract.html
    """

    if request.method == 'POST' and request.POST['email']:
        subject = '[' + request.POST['subject'] + '] ' + request.POST['name'] + ' ' + request.POST[
            'surname']

        message = '<p>' + request.POST['message'] + '</p><br/>' \
                  '<h2>Contacts</h2>' \
                  '<p>Phone: ' + request.POST['phone'] + '</p><p>Email: ' + request.POST['email'] + \
                  '</p>'

        send_mail(subject, message, settings.DEFAULT_FROM_EMAIL, [settings.EMAIL_HOST_USER], html_message=message)

        messages.info(request, "We'll get in touch soon!")  # message to display when the email has been sent

    return render(request, 'contact.html')


def self_candidacy(request):
    """
    Function to process the request made to contact the real estate agency in case of self candidacy
    :param request: the request object
    :return: an HttpResponse whose content is filled with the result of the template work-with-us.html
    """

    if request.method == 'POST' and request.POST['email']:
        subject = '[Self-candidacy] ' + request.POST['name'] + ' ' + request.POST['surname']
        message = '\nContacts\nPhone: ' + request.POST['phone'] + '\nEmail: ' + request.POST['email'] + \
                  '\n\nAdditive infos:\nCittà: ' + request.POST['city'] + '\nAddress: ' + request.POST[
                      'address'] + \
                  '\nAge: ' + request.POST['age']
        cv = request.FILES['cv']  # gets the instance of the file containing the resume

        mail = EmailMessage(subject, message, settings.DEFAULT_FROM_EMAIL, [settings.EMAIL_HOST_USER])
        mail.attach(cv.name, cv.read(), cv.content_type)  # to include the attachment
        mail.send()

        messages.info(request, "Candidacy correctly sent!")

    return render(request, 'work-with-us.html')
