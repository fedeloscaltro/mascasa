MAX_LENGTH = 100  # maximum length of string field when defining the models

# list with the general typologies of a property
TYPOLOGIES = ["Flat", "Rustics / Cascine / Houses", "Shop", "Land", "Villas"]
# list with the available types of contract
CONTRACTS = ["Sale", "Rent"]
