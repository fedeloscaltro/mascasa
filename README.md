# MaSCasa

## Descrizione
Il progetto ha come scopo la realizzazione di un sito web per un'agenzia immobiliare privata.

L'idea è di rendere disponibili diverse funzionalità a seconda del tipo di utente che desidera interfacciarsi 
con il portale.


## Software utilizzato
Il sito web è stato realizzato utilizzando 
* Python 3.8.10
* Django 3.2.4
* django-crispy-forms 1.12.0
* Pillow 8.2.0

## Installazione (necessaria versione di Python >= 3.8)
Recarsi nella cartella _release/_ in cui sono riportati gli eseguibili per i seguenti sistemi operativi.

### Windows
1. Doppio click su ```install.bat``` per installare tutte le dipendenze richieste.
2. Andare nella cartella principale del progetto e creare un file ```db.sqlite3```, che costituirà il database
3. Recarsi di nuovo in _release/_ e fare doppio click su ```run.bat```
4. Scegliere il browser preferito e aprire una scheda avente come URL ```http://127.0.0.1:8000/```

### GNU/Linux
1. Digitare a terminale ```bash install.sh``` per installare tutte le dipendenze richieste.
2. Digitare a terminale ```bash run.sh``` per lanciare l'applicazione.
3. Scegliere il browser preferito e aprire una scheda avente come URL ```http://127.0.0.1:8000/```
