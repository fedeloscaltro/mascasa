import datetime

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit
from django import forms
from .models import Property, Agent, Rating, Appointment


class InsertPropertyForm(forms.ModelForm):

    helper = FormHelper()
    helper.form_method = 'POST'
    helper.form_class = "w-75 align-items-center px-5"
    helper.add_input(Submit('submit', 'Add property', css_class="btn btn-success mt-2"))

    class Meta:
        model = Property
        fields = ['contract', 'typology', 'address', 'surface', 'price', 'image']


class UpdatePropertyForm(forms.ModelForm):

    helper = FormHelper()
    helper.form_method = 'POST'
    helper.form_class = "w-75 align-items-center px-5"
    helper.add_input(Submit('submit', 'Update property', css_class="btn btn-success mt-2"))

    class Meta:
        model = Property
        fields = ['contract', 'typology', 'address', 'surface', 'price', 'image']


class InsertAgentForm(forms.ModelForm):

    helper = FormHelper()
    helper.form_method = 'POST'
    helper.form_class = "w-75 align-items-center px-5"
    helper.add_input(Submit('submit', 'Add agent', css_class="btn btn-success mt-2"))

    class Meta:
        model = Agent
        fields = ['name', 'surname', 'email', 'phone', 'image']


class UpdateAgentForm(forms.ModelForm):

    helper = FormHelper()
    helper.form_method = 'POST'
    helper.form_class = "w-75 align-items-center px-5"
    helper.add_input(Submit('submit', 'Update agent', css_class="btn btn-success mt-2"))

    class Meta:
        model = Agent
        fields = ['name', 'surname', 'email', 'phone', 'image']


class InsertRatingForm(forms.ModelForm):

    helper = FormHelper()
    helper.form_method = 'POST'
    helper.form_class = "w-75 align-items-center px-5"
    helper.add_input(Submit('submit', 'Add review', css_class="btn btn-success mt-2"))

    class Meta:
        model = Rating
        fields = ['score', 'caption']  # the other fields are automatically set when compiling the form


class DateInput(forms.DateTimeInput):
    """
    Class to define a custom input type to use in the AppointmentForm
    """
    input_type = 'date'


class AppointmentForm(forms.ModelForm):

    helper = FormHelper()
    helper.form_method = 'POST'
    helper.form_class = "w-50 align-items-center px-5"
    helper.add_input(Submit('submit', 'Reserve', css_class="btn btn-success mt-2"))

    class Meta:
        model = Appointment
        fields = ['date']
        widgets = {
            # defines a Date-like field with the today's date as the starting feasible value
            'date': DateInput(attrs={'min': datetime.date.today()})
        }
