import datetime
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.mail import send_mail
from django.db import IntegrityError
from django.db.models import Q
from django.http import HttpResponseRedirect, HttpResponseForbidden
from django.shortcuts import render, redirect
import logging
from functools import reduce
import pandas as pd
from geopy.geocoders import Nominatim
import folium
from folium.plugins import MarkerCluster
from django.urls import reverse_lazy
from django.views.generic import CreateView, DetailView, ListView, DeleteView, UpdateView
from CasAvanguardia.constants import TYPOLOGIES, CONTRACTS
from CasAvanguardia import settings
from realestate_activity.forms import InsertPropertyForm, UpdatePropertyForm, InsertAgentForm, UpdateAgentForm, \
    InsertRatingForm, AppointmentForm
from realestate_activity.models import Property, Agent, Rating, Appointment
from users.models import Client

_logger = logging.getLogger(__name__)


def send_mail_to_users(request):
    users_email = Client.objects.filter(owner=False).values('email')
    users_email_list = [e["email"] for e in users_email]

    subject = '[MaSCasa] There\'s a new message to you!'

    message = '<h2>Announce detail</h2><br/>' \
              '<p>Contract: ' + CONTRACTS[int(request.POST['contract'])] + \
              '</p><p>Typology: ' + TYPOLOGIES[int(request.POST['typology'])] + \
              '</p><p>Address: ' + request.POST['address'] + \
              '</p><p>Surface: ' + request.POST['surface'] + \
              'm<sup>2</sup></p><p>Price: €' + request.POST['price']+'</p>'

    send_mail(subject, message, settings.DEFAULT_FROM_EMAIL, users_email_list, html_message=message)


def agent_avg_score(queryset):
    """
    Function to calculate the average score of an agent
    :param queryset: the object containing all the rating related to an agent
    :return: the average score based on the number of rating
    """
    if len(queryset) > 0:
        list_score = [q.score for q in queryset]
        sum_score = reduce(lambda a, b: a + b, list_score)
        return round(sum_score / len(queryset), 2)
    else:
        return 0


def order_results(order_price, order_surface, order_data):
    """
    Function to show the results of a search order by multiple aspects
    :param order_price: string filled with one between 'Crescente' and 'Decrescente' and '----'
    :param order_surface: same as for order_price but this time based on the squared meters of a property
    :param order_data: string filled with one between 'Più recenti' and 'Meno recenti' and '----'
    :return: list containing the order parameters to use to show the results
    """
    order_fields = ['price', 'surface', 'insertion_date']
    order_values = [order_price, order_surface, order_data]
    if order_price == '----':
        order_fields = order_fields[1:]
        order_values = order_values[1:]
    if order_surface == '----':
        order_fields = [order_fields[i] for i in [0, len(order_fields) - 1]]
        order_values = [order_values[i] for i in [0, len(order_values) - 1]]
    if order_data == '----':
        order_fields = order_fields[:len(order_fields) - 1]
        order_values = order_values[:len(order_values) - 1]

    for i, val in enumerate(order_values):
        if val == 'Decreasing' or val == 'Most recent':
            order_fields[i] = '-' + order_fields[i]  # to set a descending order

    return order_fields


def search_properties(self):
    """
    Function which implements the search through all the properties
    :param self: the instance of the view
    :return: a Queryset object with the searched items in the desired order
    """
    contract_filter = self.request.GET.get('contract')
    typology_filter = self.request.GET.get('typology')
    min_surface = int(self.request.GET.get('minSurface'))
    max_surface = int(self.request.GET.get('maxSurface'))
    min_price = int(self.request.GET.get('minPrice'))
    max_price = int(self.request.GET.get('maxPrice'))
    order_price = self.request.GET.get('orderPrice')
    order_surface = self.request.GET.get('orderArea')
    order_data = self.request.GET.get('orderData')

    base_queryset = Property.objects.filter(surface__range=[min_surface, max_surface],
                                            price__range=[min_price, max_price])

    if contract_filter == 'All':
        if typology_filter == 'All':
            queryset = base_queryset
        else:
            typology_filter = str(TYPOLOGIES.index(typology_filter))
            queryset = base_queryset.filter(typology=typology_filter)
    elif typology_filter == 'All':
        contract_filter = str(CONTRACTS.index(contract_filter))
        queryset = base_queryset.filter(contract=contract_filter)
    else:
        typology_filter = str(TYPOLOGIES.index(typology_filter))
        contract_filter = str(CONTRACTS.index(contract_filter))
        queryset = base_queryset.filter(contract=contract_filter,
                                        typology=typology_filter)

    order_fields = order_results(order_price, order_surface, order_data)

    return queryset.order_by(*order_fields)


def related_items(detail_object):
    """
    Function to realise a simple recommendation system based on some property parameters
    :param detail_object: the property the user is interested
    :return: an object containing the properties related to one property
    """

    # max: actual+20%; min: actual-20%
    max_price = int(detail_object.price + detail_object.price / 5)
    min_price = int(detail_object.price - detail_object.price / 5)
    min_surface = int(detail_object.surface - detail_object.surface / 5)
    max_surface = int(detail_object.surface + detail_object.surface / 5)

    base_related_items = Property.objects.filter(contract=detail_object.contract,
                                                 price__range=[min_price, max_price],
                                                 surface__range=[min_surface, max_surface])

    rel_items = base_related_items.filter(Q(id_i__gt=detail_object.id_i) |
                                          Q(id_i__lt=detail_object.id_i))  # to exclude the detail_object itself
    return rel_items


def create_map(df, zoom):
    """
    Function to return a map object to render in the preferred template
    :param df: a Dataframe containing the geo-localized addresses
    :param zoom: an int value to automatically zoom over the markers
    :return: a HTML representation of the processed map
    """
    mmap = folium.Map(location=df[["lat", "lon"]].mean().to_list(), zoom_start=zoom)

    marker_cluster = MarkerCluster().add_to(mmap)  # object to put the markers on
    for i, r in df.iterrows():
        location = (r["lat"], r["lon"])
        folium.Marker(location=location,
                      popup=r["address"],
                      tooltip=r["address"]).add_to(marker_cluster)

    mmap = mmap._repr_html_()  # the HTML representation of the map
    return mmap


def render_map(context):
    """
    Function to pass a map in a specified field of the context object
    :param context: the context object
    :return: the context with the 'map' key pointing the the HTML representation of the map to render
    """
    try:
        actual_address_list = [o.address for o in context["object_list"]]
        zoom = 12
    # the function is called either when retrieving just a single address (DetailView) and list o addresses (ListView)
    except KeyError:
        actual_address_list = [context["object"].address]
        zoom = 17  # higher zoom when there's just one object to display

    df = make_dataframe(actual_address_list)

    marker_map = create_map(df, zoom)
    context["map"] = marker_map

    return context


def make_dataframe(address_list):
    """
    Function to prepare the Dataframe containing the geo-localised addresses
    :param address_list: the list containing the addresses in string-format
    :return: the Dataframe instance
    """
    # if the object is not a list then is useless to keep processing it
    assert type(address_list) == list, AssertionError

    df = pd.DataFrame(address_list)
    df.columns = ['address']

    geolocator = Nominatim(user_agent="MasCasa app")  # instances the geolocator using the OpenStreetMap API

    df["loc"] = df["address"].apply(geolocator.geocode)
    df["point"] = df["loc"].apply(lambda loc: tuple(loc.point) if loc else None)
    df[['lat', 'lon', 'altitude']] = pd.DataFrame(df['point'].to_list(), index=df.index)

    return df


@login_required
def sell_request(request):
    """
    Function to process the request made to sell a property
    :param request: the request object
    :return: an HttpResponse whose content is filled with the result of the template sell-house.html
    """
    if request.method == 'POST' and request.POST['email']:
        subject = '['+ request.POST['purpose'] + ' Request] ' + request.POST['name'] + ' ' + request.POST['surname']

        message = '<h1>Contacts</h1>' \
                  '<p>Phone: ' + request.POST['phone'] + '</p><p>Email: ' + request.POST['email'] + \
                  '</p><h2>Additive infos:</h2><p>Indirizzo: ' + request.POST['address'] + '</p>' \
                                                                                                    '<p>Residential area: ' + \
                  request.POST['scope'] + \
                  '</p><p>Specific message: ' + request.POST['message'] + '</p>'

        send_mail(subject, message, settings.DEFAULT_FROM_EMAIL, [settings.EMAIL_HOST_USER], html_message=message)

        messages.info(request, "Request correctly sent!")

    return render(request, 'realestate_activity/sell-house.html')


@login_required
def rate_request(request):
    """
    Function to process the request to have a free evaluation of a property
    :param request: the request object
    :return: an HttpResponse whose content is filled with the result of the template rate-house.html
    """
    if request.method == 'POST' and request.POST['email']:
        subject = '[Evaluation Request] ' + request.POST['name'] + ' ' + request.POST['surname']

        message = '<h1>Contacts</h1>' \
                  '<p>Phone: ' + request.POST['phone'] + '</p><p>Email: ' + request.POST['email'] + \
                  '</p><h2>Additive infos:</strong></h2>' + \
                  '<p>typology: ' + request.POST['scope'] + '</p><p>Surface: ' + request.POST['surface'] + 'm^2</p>'

        send_mail(subject, message, settings.DEFAULT_FROM_EMAIL, [settings.EMAIL_HOST_USER], html_message=message)

        messages.info(request, "Request correctly sent!")

    return render(request, 'realestate_activity/rate-house.html')


class InsertProperty(LoginRequiredMixin, CreateView):
    form_class = InsertPropertyForm
    model = Property
    template_name = 'realestate_activity/CRUD/insert-property.html'

    def get_success_url(self):
        send_mail_to_users(self.request)

        prop = Property.objects.last()
        return prop.get_absolute_url()

    def dispatch(self, request, *args, **kwargs):
        if self.request.user.owner:
            return super(InsertProperty, self).dispatch(request, *args, **kwargs)
        else:
            return HttpResponseForbidden("403 - Access denied")


class PropertyDetail(DetailView):
    model = Property
    template_name = 'realestate_activity/CRUD/detail-property.html'

    def get_context_data(self, **kwargs):
        context = super(PropertyDetail, self).get_context_data(**kwargs)

        context['contract'] = CONTRACTS[int(context['object'].contract)]
        context['typology'] = TYPOLOGIES[int(context['object'].typology)]

        detail_object = context["object"]

        context["rel_items"] = related_items(detail_object)

        for item in context["rel_items"]:
            item.typo = TYPOLOGIES[int(item.typology)]
            item.contr = CONTRACTS[int(item.contract)]

        context = render_map(context)

        return context


class DeleteDetail(LoginRequiredMixin, DetailView, DeleteView):
    model = Property
    template_name = 'realestate_activity/CRUD/delete-property.html'
    success_url = reverse_lazy('activity:all-properties')

    def get_context_data(self, **kwargs):
        context = super(DeleteDetail, self).get_context_data(**kwargs)

        context["object"].typo = TYPOLOGIES[int(context["object"].typology)]
        context["object"].contr = CONTRACTS[int(context["object"].contract)]

        return context

    def dispatch(self, request, *args, **kwargs):
        if self.request.user.owner:
            return super(DeleteDetail, self).dispatch(request, *args, **kwargs)
        else:
            return HttpResponseForbidden("403 - Access denied")


class UpdateDetail(LoginRequiredMixin, DetailView, UpdateView):
    model = Property
    template_name = 'realestate_activity/CRUD/update-property.html'
    form_class = UpdatePropertyForm

    def get_success_url(self):
        # redirect to the property once it's updated
        return reverse_lazy('activity:detail-property', kwargs={'pk': self.kwargs['pk']})

    def dispatch(self, request, *args, **kwargs):
        if self.request.user.owner:
            return super(UpdateDetail, self).dispatch(request, *args, **kwargs)
        else:
            return HttpResponseForbidden("403 - Access denied")


class AllProperties(ListView):
    model = Property
    template_name = 'realestate_activity/CRUD/all-properties.html'

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(AllProperties, self).get_context_data(**kwargs)

        if self.request.method == 'GET' and self.request.GET.get('contract') is not None:
            context['object_list'] = search_properties(self)

        for o in context['object_list']:
            o.typo = TYPOLOGIES[int(o.typology)]
            o.contr = CONTRACTS[int(o.contract)]

        if len(context['object_list']) > 0:
            context = render_map(context)

        return context


class InsertAgent(LoginRequiredMixin, CreateView):
    form_class = InsertAgentForm
    model = Agent
    template_name = 'realestate_activity/CRUD/insert-agent.html'

    def get_success_url(self):
        agent = Agent.objects.last()
        return agent.get_absolute_url()

    def dispatch(self, request, *args, **kwargs):
        if self.request.user.owner:
            return super(InsertAgent, self).dispatch(request, *args, **kwargs)
        else:
            return HttpResponseForbidden("403 - Access denied")


class AgentDetail(DetailView):
    model = Agent
    template_name = 'realestate_activity/CRUD/detail-agent.html'

    def get_context_data(self, **kwargs):
        context = super(AgentDetail, self).get_context_data(**kwargs)
        queryset = Rating.objects.filter(fk_a=context["object"].id_a)
        context['rating_list'] = queryset

        context['avg_score'] = agent_avg_score(queryset)
        return context


class Agents(ListView):
    model = Agent
    template_name = 'realestate_activity/CRUD/all-agents.html'

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(Agents, self).get_context_data(**kwargs)

        for obj in context["object_list"]:
            queryset = Rating.objects.filter(fk_a=obj.id_a)
            obj.avg_score = agent_avg_score(queryset)

        return context


class DeleteAgent(LoginRequiredMixin, DetailView, DeleteView):
    model = Agent
    template_name = 'realestate_activity/CRUD/delete-agent.html'
    success_url = reverse_lazy('activity:agents')

    def dispatch(self, request, *args, **kwargs):
        if self.request.user.owner:
            return super(DeleteAgent, self).dispatch(request, *args, **kwargs)
        else:
            return HttpResponseForbidden("403 - Access denied")


class UpdateAgent(LoginRequiredMixin, DetailView, UpdateView):
    model = Agent
    template_name = 'realestate_activity/CRUD/update-agent.html'
    form_class = UpdateAgentForm

    def get_success_url(self):
        return reverse_lazy('activity:agent-detail', kwargs={'pk': self.kwargs['pk']})

    def dispatch(self, request, *args, **kwargs):
        if self.request.user.owner:
            return super(UpdateAgent, self).dispatch(request, *args, **kwargs)
        else:
            return HttpResponseForbidden("403 - Access denied")


class AppointmentView(LoginRequiredMixin, CreateView, ListView):
    form_class = AppointmentForm
    model = Appointment
    template_name = 'realestate_activity/get-appointment.html'
    success_url = 'appointment'

    def form_valid(self, form):
        form.instance.fk_u = self.request.user
        try:
            return super().form_valid(form)
        except IntegrityError:
            messages.info(self.request, "You already got an appointment that day!")
            return HttpResponseRedirect('appointment')

    def get_context_data(self, **kwargs):
        context = super(AppointmentView, self).get_context_data()
        obj_list = []

        for c in context['object_list'].values():
            if c['fk_u_id'] == self.request.user.pk and c['date'] >= datetime.date.today():
                obj_list.append(c)

        context['object_list'] = obj_list  # to obtain just the appointments currently available

        return context


class DeleteAppointmentView(LoginRequiredMixin, ListView):
    model = Appointment
    template_name = 'realestate_activity/delete-appointment.html'

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(DeleteAppointmentView, self).get_context_data()
        context["deletion"] = False  # to detect if the user selected an appointment to delete
        obj_list = []

        for c in context['object_list'].values():
            if c['fk_u_id'] == self.request.user.pk and c['date'] >= datetime.date.today():
                obj_list.append(c)

        context['object_list'] = obj_list  # to obtain just the appointments of the current user

        for i, c in enumerate(context['object_list']):
            if self.request.GET.get('radioButton' + str(i + 1)):
                Appointment.objects.filter(fk_u=self.request.user, date=c["date"]).delete()
                context["deletion"] = True

        return context

    def render_to_response(self, context, **response_kwargs):
        if context["deletion"]:
            # if the user has selected at least one appointment to cancel
            return redirect(reverse_lazy('appointment'))

        return super(DeleteAppointmentView, self).render_to_response(context, **response_kwargs)


class RatingView(LoginRequiredMixin, CreateView):
    model = Rating
    template_name = 'realestate_activity/rating.html'
    form_class = InsertRatingForm

    def get_success_url(self):
        return reverse_lazy('activity:agent-detail', kwargs={'pk': self.kwargs['pk']})

    def get_context_data(self, **kwargs):
        context = super(RatingView, self).get_context_data(**kwargs)
        context["agent"] = Agent.objects.get(pk=self.kwargs["pk"])

        return context

    def form_valid(self, form):
        form.instance.fk_u_id = self.request.user.id
        form.instance.fk_a_id = Agent.objects.get(pk=self.kwargs["pk"]).id_a
        try:
            return super(RatingView, self).form_valid(form)
        except IntegrityError:
            messages.info(self.request, f"You cannot review {Agent.objects.get(pk=self.kwargs['pk']).name} again today!")
            return HttpResponseRedirect('recensione-agente')
