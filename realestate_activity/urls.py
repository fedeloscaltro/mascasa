from django.urls import path
from realestate_activity.views import *

app_name = 'activity'  # all the names in urlpattern will be preceded by this value

urlpatterns = [
    path('agenti', Agents.as_view(), name="agents"),
    path('vendita', sell_request, name="sell-house"),
    path('valutazione', rate_request, name="rate-house"),
    path('inserisci-annuncio', InsertProperty.as_view(), name="insert-property"),
    path('<int:pk>/detail', PropertyDetail.as_view(), name="detail-property"),
    path('<int:pk>/update', UpdateDetail.as_view(), name="update-property"),
    path('<int:pk>/delete', DeleteDetail.as_view(), name="delete-property"),
    path('lista', AllProperties.as_view(), name="all-properties"),
    path('inserisci-agente', InsertAgent.as_view(), name="insert-agent"),
    path('<int:pk>/agent-detail', AgentDetail.as_view(), name="agent-detail"),
    path('<int:pk>/agent-delete', DeleteAgent.as_view(), name="delete-agent"),
    path('<int:pk>/agent-update', UpdateAgent.as_view(), name="update-agent"),
    path('<int:pk>/recensione-agente', RatingView.as_view(), name="rate-agent"),
    path('appointment', AppointmentView.as_view(), name='appointment'),
    path('appointment/delete-appointments', DeleteAppointmentView.as_view(), name='delete-appointment'),
]
