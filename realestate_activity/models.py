from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator
from django.urls import reverse
from phonenumber_field.modelfields import PhoneNumberField
from users.models import Client

from CasAvanguardia.constants import *


class Property(models.Model):
    contracts = [(str(i), CONTRACTS[i]) for i in range(len(CONTRACTS))]
    typology_list = [(str(i), TYPOLOGIES[i]) for i in range(len(TYPOLOGIES))]

    id_i = models.AutoField(primary_key=True)
    image = models.ImageField(upload_to='images/')
    address = models.CharField(max_length=MAX_LENGTH, default="")
    contract = models.CharField(max_length=MAX_LENGTH, choices=contracts, default=contracts[0])
    typology = models.CharField(max_length=MAX_LENGTH, choices=typology_list)
    surface = models.IntegerField(validators=[MinValueValidator(1)])
    price = models.IntegerField(validators=[MinValueValidator(1)])
    insertion_date = models.DateField(auto_now=True)

    class Meta:
        verbose_name_plural = 'Properties'

    def get_absolute_url(self):
        return reverse('activity:detail-property', kwargs={'pk': self.pk})


class Agent(models.Model):
    id_a = models.AutoField(primary_key=True)
    name = models.CharField(max_length=MAX_LENGTH)
    surname = models.CharField(max_length=MAX_LENGTH)
    phone = PhoneNumberField()
    email = models.EmailField()
    image = models.ImageField(upload_to='images/agents/', default='images/agents/agent-1.jpg')

    def get_absolute_url(self):
        return reverse('activity:agent-detail', kwargs={'pk': self.pk})


class Rating(models.Model):
    caption = models.TextField(null=True)
    score = models.IntegerField(validators=[MinValueValidator(0), MaxValueValidator(5)])
    rating_date = models.DateField(auto_now=True)
    fk_a = models.ForeignKey(Agent, related_name="rating", on_delete=models.CASCADE)
    fk_u = models.ForeignKey(Client, related_name="rating", on_delete=models.CASCADE)

    class Meta:
        # A user cannot publish a rating over the same agent twice a day
        constraints = [models.UniqueConstraint(fields=['fk_a', 'fk_u', 'rating_date'], name='unique_rating')]


class Appointment(models.Model):
    date = models.DateField()
    fk_u = models.ForeignKey(Client, related_name="appointment", on_delete=models.CASCADE)

    class Meta:
        constraints = [
            # A user cannot take more than one appointment in a specific date
            models.UniqueConstraint(fields=['fk_u', 'date'], name='unique_appointment'),
        ]
