import datetime
from django.contrib import admin
from django import forms

from .models import Agent, Appointment, Property, Rating


@admin.register(Property)
class PropertyAdmin(admin.ModelAdmin):
    list_display = ["address", "contract", "typology", "surface", "price", "insertion_date"]
    list_filter = ["contract", "typology", "insertion_date"]
    search_fields = ["address"]

    def get_form(self, request, obj=None, change=False, **kwargs):
        form = super(PropertyAdmin, self).get_form(request, obj, **kwargs)
        form.base_fields["typology"].label = "Category of property"  # to customise the label of the typology field
        return form


class AppointmentAdminForm(forms.ModelForm):
    class Meta:
        model = Appointment
        fields = ["fk_u", "date"]

    def clean_date(self):
        if self.cleaned_data["date"] < datetime.date.today():
            raise forms.ValidationError("You can't insert a past date")

        return self.cleaned_data["date"]


@admin.register(Appointment)
class AppointmentAdmin(admin.ModelAdmin):
    list_display = ["fk_u", "date"]
    form = AppointmentAdminForm


@admin.register(Rating)
class RatingAdmin(admin.ModelAdmin):
    list_display = ["fk_a", "fk_u", "score"]
    list_filter = ["fk_a", "fk_u", "rating_date"]


@admin.register(Agent)
class AgentAdmin(admin.ModelAdmin):
    list_display = ["name", "surname", "phone", "email"]

    def get_form(self, request, obj=None, change=False, **kwargs):
        form = super(AgentAdmin, self).get_form(request, obj, change, **kwargs)
        form.base_fields["image"].label = "Profile picture"
        return form
