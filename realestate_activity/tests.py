from django.core import mail
from django.db import IntegrityError
from django.test import TestCase, Client
from django.urls import reverse_lazy

from .models import Rating, Agent, Client as customClient, Appointment
from .views import make_dataframe


class RatingTests(TestCase):
    """
    Class to inspect the rating workflow
    """
    def setUp(self):
        self.data = {
            'fk_u_id': 1,
            'fk_a_id': 1,
            'rating_date': '2021-07-21',
            'caption': 'A good review',
            'score': 4
        }
        self.custom_user = customClient.objects.create_user(username='marietto', password='mariogigetto',
                                                            email='mail@gmail.com', phone='+393408680099',
                                                            address='Via Panzerotto 15, Reggio Emilia')

        self.custom_agent = Agent.objects.create(email='agente1@gmail.com', name='Marcello', surname='Marmiroli',
                                                 image='media/images/agents/agent-1.jpg')

    def test_invalid_rate_integrity_error(self):
        """
        Test to check the constraint over the model Rating
        :return: OK if an IntegrityError has been raised
        """
        Rating(**self.data).save()
        duplicated_rate = Rating(fk_u_id=1, fk_a_id=1, rating_date='2021-07-21',
                                 caption='A good review', score=5)
        with self.assertRaises(IntegrityError) as raised:
            duplicated_rate.save()
        self.assertEqual(IntegrityError, type(raised.exception))

    def test_invalid_grater_rate(self):
        """
        Test to check the constraint over score value of a rating.
        Use case when the value is grater than the maximum consented one
        :return: OK if the response contains the class is-invalid given by the app crispy-form
        """
        client = Client()
        response = client.login(username="marietto", password="mariogigetto")
        self.assertTrue(response)

        response = client.post(reverse_lazy('activity:rate-agent', kwargs={'pk': self.custom_user.pk}),
                               {
                                   'fk_u_id': 1,
                                   'fk_a_id': 1,
                                   'rating_date': '2021-07-20',
                                   'caption': 'A new review',
                                   'score': 7
                               }, follow=True)
        self.assertEqual(response.status_code, 200)

        self.assertContains(response, 'is-invalid')

    def test_invalid_lower_rate(self):
        """
        Test to check the constraint over score value of a rating.
        Use case when the value is lower than the minimum consented one
        :return: OK if the response contains the class is-invalid given by the app crispy-form
        """
        client = Client()
        response = client.login(username="marietto", password="mariogigetto")
        self.assertTrue(response)

        response = client.post(reverse_lazy('activity:rate-agent', kwargs={'pk': self.custom_user.pk}),
                               {
                                   'fk_u_id': 1,
                                   'fk_a_id': 1,
                                   'rating_date': '2021-07-20',
                                   'caption': 'A new review',
                                   'score': -2
                               }, follow=True)
        self.assertEqual(response.status_code, 200)

        self.assertContains(response, 'is-invalid')

    def test_valid_rate(self):
        """
        Test to check the constraint over score value of a rating.
        Use case when the value within the consented range
        :return: OK if the response's context contains one rating
        """
        client = Client()
        response = client.login(username="marietto", password="mariogigetto")
        self.assertTrue(response)

        response = client.post(reverse_lazy('activity:rate-agent', kwargs={'pk': self.custom_user.pk}),
                               {
                                   'fk_u_id': 1,
                                   'fk_a_id': 1,
                                   'rating_date': '2021-07-20',
                                   'caption': 'A new review',
                                   'score': 4
                               }, follow=True)
        self.assertEqual(response.status_code, 200)

        response = client.get(reverse_lazy('activity:agent-detail', kwargs={'pk': self.custom_agent.pk}))
        self.assertEqual(len(response.context['rating_list']), 1)


class EmailTest(TestCase):
    """
    Class to inspect the settings to send emails
    """
    def setUp(self):
        self.subject = '[Richiesta valutazione] Mario Rossi'
        self.message = '<h1>Contacts</h1>' \
                       '<p>Phone: +393484044444 </p><p>Email: mario@rossi.it'
        self.sender = 'from@example.com'
        self.receiver_list = ['to@example.com']

    def test_send_email(self):
        """
        Test to check if the email is sent or not
        :return: OK if the outbox contains 1 mail, has the specified subject and as no attachments
        """
        mail.send_mail(self.subject, self.message, self.sender, self.receiver_list,
                       fail_silently=False)

        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].subject, '[Evaluation Request] Mario Rossi')
        self.assertEqual(mail.outbox[0].attachments, [])


class AppointmentTest(TestCase):
    """
    Class to inspect the appointment workflow
    """
    def setUp(self):
        self.data = {
            'date': '2021-07-21',
            'fk_u_id': 1
        }

        self.custom_user = customClient.objects.create_user(username='marietto', password='mariogigetto',
                                                            email='mail@gmail.com', phone='+393408680099',
                                                            address='Via Panzerotto 15, Reggio Emilia')

    def test_invalid_appointment_integrity_error(self):
        """
        Test to check the constraint over the model Appointment
        :return: OK if an IntegrityError has been raised
        """
        Appointment(**self.data).save()
        second_appointment = Appointment(fk_u_id=1, date='2021-07-21')

        with self.assertRaises(IntegrityError) as raised:
            second_appointment.save()
        self.assertEqual(IntegrityError, type(raised.exception))


class DataframeTest(TestCase):
    """
    Class to inspect the beheavior of the make_dataframe function used when rendering the map
    """
    def setUp(self):
        self.address_list = ['Via Roma 2, Reggio Emilia',
                             'Via Roma 11, Reggio Emilia',
                             'Via Carlo Calvi di Coenzo 12, Reggio Emilia']

    def test_consistent_dataframe(self):
        """
        Test to check if the geo-places are set when entering valid addresses
        :return: OK if all the addresses have been localized
        """
        df = make_dataframe(self.address_list)
        self.assertEqual(len(df.values), len(self.address_list))

    def test_make_dataframe_with_not_existing_street(self):
        """
        Test to check if a ValueError is raised when the input is at least one not-existing address
        :return: OK if a ValueError exception is raised
        """
        with self.assertRaises(ValueError) as raised:
            make_dataframe(["Via Bizzarra 4, Reggio Emilia"])
            self.assertEqual(ValueError, type(raised.exception))

    def test_make_dataframe_with_wrong_data_input_type(self):
        """
        Test to check functionality of the assertion inserted into the make_dataframe function
        :return: OK if an AssertionError exception is raised
        """
        try:
            make_dataframe("Via Roma 2, Reggio Emilia")
        except Exception as e:
            self.assertEqual(type(e), AssertionError)

    def test_make_dataframe_with_empty_list(self):
        """
        Test to check if a ValueError is raised when the input is an empty list
        :return: OK if a ValueError exception is raised
        """
        try:
            make_dataframe([])
            self.fail("Empty list!")
        except ValueError:
            pass
