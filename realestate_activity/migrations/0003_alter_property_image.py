# Generated by Django 3.2.4 on 2021-06-29 15:19

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('realestate_activity', '0002_auto_20210628_1844'),
    ]

    operations = [
        migrations.AlterField(
            model_name='property',
            name='image',
            field=models.ImageField(upload_to=''),
        ),
    ]
