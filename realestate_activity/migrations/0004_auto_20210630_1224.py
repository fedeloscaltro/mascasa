# Generated by Django 3.2.4 on 2021-06-30 10:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('realestate_activity', '0003_alter_property_image'),
    ]

    operations = [
        migrations.AddField(
            model_name='property',
            name='insertion_date',
            field=models.DateField(auto_now=True),
        ),
        migrations.AlterField(
            model_name='property',
            name='image',
            field=models.ImageField(upload_to='images/'),
        ),
    ]
