from django.apps import AppConfig


class RealestateActivityConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'realestate_activity'
